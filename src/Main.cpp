/*
 *
 * Copyright (C) 2010-2013  Kaz Nishimura
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <vcl.h>
#pragma hdrstop

#include <initguid.h>
#include "Main.h"
#pragma package(smart_init)

#include "About.h"
#include "Strings.hpp"
#include <commctrl.h>
#include <algorithm>
#include <functional>
#include <iterator>
#include <cassert>

#pragma comment(lib, "msdmo")

using namespace std;
using boost::scoped_ptr;
#if __cplusplus >= 201103L
using namespace std::placeholders;
#endif

#pragma resource "*.dfm"
TDMOListForm *DMOListForm;

/*
 * ## TDMOListForm implementation
 */

__fastcall TDMOListForm::TDMOListForm(TComponent *Owner)
    : inherited(Owner),
      MediaTypeDirectory(new TMediaTypeDirectory),
      worker(0) {

    TVarRec formatArgs[] = {Strings_ProductName};
    Caption = Format(Caption, formatArgs, 0);
    Contents1->Caption = Format(Contents1->Caption, formatArgs, 0);
    About1->Caption = Format(About1->Caption, formatArgs, 0);

    sortColumnIndex = 0;
    sortDescending = false;
    UpdateListView1Header();

    // Refresh is called later to make it sure that the destructor is called
    // properly.
}

__fastcall TDMOListForm::~TDMOListForm() {
    delete worker;
}

void __fastcall TDMOListForm::AfterConstruction() {
    inherited::AfterConstruction();
    Refresh();
}

void __fastcall TDMOListForm::Refresh() {
    delete worker;

    ListView1->Items->Clear();

    worker = new TRefreshThread(this, true);
    worker->OnTerminate = RefreshThreadTerminate;
    worker->FreeOnTerminate = true;
    worker->Start();
}

void __fastcall TDMOListForm::RefreshThreadTerminate(TObject *Sender) {
    worker = 0;
}

void __fastcall TDMOListForm::AboutActionExecute(TObject *Sender) {
    scoped_ptr<TAboutBox> AboutBox1(new TAboutBox(0));
    AboutBox1->ShowModal();
}

void __fastcall TDMOListForm::ListView1Compare(TObject *Sender,
    TListItem *Item1, TListItem *Item2, int Data, int &Compare) {

    if (sortColumnIndex == 0) {
        Compare = CompareText(Item1->Caption, Item2->Caption);
    } else {
        Compare = CompareText(Item1->SubItems->Strings[sortColumnIndex - 1],
            Item2->SubItems->Strings[sortColumnIndex - 1]);
    }

    if (sortDescending)
        Compare = -Compare;
}

void __fastcall TDMOListForm::ListView1ColumnClick(TObject *Sender,
    TListColumn *Column) {

    if (sortColumnIndex == Column->Index) {
        sortDescending = !sortDescending;
    } else {
        sortColumnIndex = Column->Index;
        sortDescending = false;
    }
    UpdateListView1Header();
    ListView1->AlphaSort();
}

void __fastcall TDMOListForm::RefreshActionExecute(TObject *Sender) {
    Refresh();
}

void __fastcall TDMOListForm::UpdateListView1Header() {
    HWND hwndHeader = ListView_GetHeader(ListView1->Handle);
    if (hwndHeader != 0) {
        int n = Header_GetItemCount(hwndHeader);
        if (n >= 0) {
            for (int i = 0; i != n; ++i) {
                HDITEM item;
                item.mask = HDI_FORMAT;
                if (Header_GetItem(hwndHeader, i, &item)) {
                    int newfmt = item.fmt & ~(HDF_SORTDOWN | HDF_SORTUP);
                    if (i == sortColumnIndex) {
                        if (sortDescending) {
                            newfmt |= HDF_SORTDOWN;
                        } else {
                            newfmt |= HDF_SORTUP;
                        }
                    }
                    // If the new format differs from the current one, set it.
                    if (newfmt != item.fmt) {
                        item.mask = HDI_FORMAT;
                        item.fmt = newfmt;
                        Header_SetItem(hwndHeader, i, &item);
                    }
                }
            }
        }
    }
}

void TDMOListForm::add(const Item &item) {
    ListView1->Items->BeginUpdate();

    TListItem *listItem1 = ListView1->Items->Add();
    listItem1->Caption = item.name;
    listItem1->SubItems->Add(GUIDToString(item.clsid));

    if (!item.inputTypes.empty()) {
        TMediaType mediaType =
            MediaTypeDirectory->Lookup(item.inputTypes[0].type,
            item.inputTypes[0].subtype);
        listItem1->SubItems->Add(mediaType.Name);
    } else
        listItem1->SubItems->Add("");

    if (!item.outputTypes.empty()) {
        TMediaType mediaType =
            MediaTypeDirectory->Lookup(item.outputTypes[0].type,
            item.outputTypes[0].subtype);
        listItem1->SubItems->Add(mediaType.Name);
    } else
        listItem1->SubItems->Add("");

    ListView1->Items->EndUpdate();
}

/*
 * ## TWorkerThread implementation
 */

__fastcall TRefreshThread::TRefreshThread(TDMOListForm *form, bool suspended)
        : TThread(suspended),
          ownerForm(form) {
    assert(form != 0);
    SyncObj = new TCriticalSection();
}

__fastcall TRefreshThread::~TRefreshThread() {
    delete SyncObj;
}

void __fastcall TRefreshThread::AddItems() {
    vector<TDMOListForm::Item> items;

    // Items are moved to a local variable while locking.
    SyncObj->Acquire();
    __try {
        items.swap(QueuedItems);
    } __finally {
        SyncObj->Release();
    }

    // The actual update may be performed with no lock.
    for_each(items.begin(), items.end(), bind(&TDMOListForm::add,
            ownerForm, _1));
}

void __fastcall TRefreshThread::Execute() {
    IEnumDMO *e = 0;
    HRESULT result = DMOEnum(GUID_NULL, 0, 0, 0, 0, 0,&e);
    if (!SUCCEEDED(result)) {
        return;
    }

    CLSID clsid[1];
    WCHAR *name[1];
    while (e->Next(1, clsid, name, 0) == S_OK) {
        DMO_PARTIAL_MEDIATYPE input[1], output[1];
        unsigned long nInput = 0, nOutput = 0;
        DMOGetTypes(clsid[0], 1, &nInput, input, 1, &nOutput, output);

        TDMOListForm::Item item;
        item.clsid = clsid[0];
        item.name = name[0];
        item.inputTypes.clear();
        copy(input, input + nInput, back_inserter(item.inputTypes));
        item.outputTypes.clear();
        copy(output, output + nOutput, back_inserter(item.outputTypes));

        SyncObj->Acquire();
        __try {
            if (QueuedItems.empty()) {
                Queue(&AddItems);
            }
            QueuedItems.push_back(item);
        } __finally {
            SyncObj->Release();
        }
    }

    RemoveQueuedEvents(&AddItems);
    // Note that invoking AddItems for an empty queue is safe.
    Synchronize(&AddItems);
}
