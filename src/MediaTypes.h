/*
 * MediaTypes - media type information (interface)
 * Copyright (C) 2010-2013  Kaz Nishimura
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MediaTypesH
#define MediaTypesH 1

#include <System.Classes.hpp>
#include <System.IniFiles.hpp>
#include <boost/scoped_ptr.hpp>

namespace Mediatypes {
    struct TMediaType {
        UnicodeString Name;
    };

    class TMediaTypeDirectory : public TObject {
        typedef TObject inherited;

    public:
        __fastcall TMediaTypeDirectory();

        TMediaType __fastcall Lookup(REFGUID Type, REFGUID Subtype);

    protected:
        static UnicodeString __fastcall GetIniFileName();

    private:
        boost::scoped_ptr<TIniFile> IniFile;
    };
}
using namespace Mediatypes;

#endif
