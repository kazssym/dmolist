object DMOListForm: TDMOListForm
  Left = 0
  Top = 0
  Caption = '%s'
  ClientHeight = 292
  ClientWidth = 426
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 15
  object ListView1: TListView
    Left = 0
    Top = 0
    Width = 426
    Height = 292
    Align = alClient
    Columns = <
      item
        Caption = 'Name'
        Width = 100
      end
      item
        Caption = 'CLSID'
        Width = 100
      end
      item
        Caption = 'Input type'
        Width = 100
      end
      item
        Caption = 'Output type'
        Width = 100
      end>
    SortType = stData
    TabOrder = 0
    ViewStyle = vsReport
    OnColumnClick = ListView1ColumnClick
    OnCompare = ListView1Compare
  end
  object ActionList1: TActionList
    Left = 32
    Top = 32
    object FileExit1: TFileExit
      Category = 'File'
      Caption = 'E&xit'
      Hint = 'Exit|Quits the application'
      ImageIndex = 43
    end
    object RefreshAction: TAction
      Category = 'View'
      Caption = '&Refresh'
      OnExecute = RefreshActionExecute
    end
    object HelpContents1: THelpContents
      Category = 'Help'
      Caption = '&Contents'
      ImageIndex = 40
    end
    object AboutAction: TAction
      Category = 'Help'
      Caption = '&About'
      Hint = 'About|Describes the application'
      OnExecute = AboutActionExecute
    end
  end
  object MainMenu1: TMainMenu
    Left = 112
    Top = 32
    object File1: TMenuItem
      Caption = '&File'
      object Exit1: TMenuItem
        Action = FileExit1
      end
    end
    object View1: TMenuItem
      Caption = '&View'
      object Refresh1: TMenuItem
        Action = RefreshAction
      end
    end
    object Help1: TMenuItem
      Caption = '&Help'
      object Contents1: TMenuItem
        Action = HelpContents1
        Caption = '%s &help'
      end
      object HelpN1: TMenuItem
        Caption = '-'
      end
      object About1: TMenuItem
        Action = AboutAction
        Caption = '&About %s'
      end
    end
  end
end
