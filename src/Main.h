/*
 *
 * Copyright (C) 2010-2013  Kaz Nishimura
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MainH
#define MainH 1

#include "MediaTypes.h"
#include <dmo.h>
#include <System.Classes.hpp>
#include <System.Actions.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ActnList.hpp>
#include <Vcl.StdActns.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.ComCtrls.hpp>
#include <boost/scoped_ptr.hpp>
#include <vector>

/*
 * Main form of this program.
 */
class TDMOListForm : public TForm {
    typedef TForm inherited;

__published:
    TListView *ListView1;

    // Actions
    TActionList *ActionList1;
    TFileExit *FileExit1;
    TAction *RefreshAction;
    THelpContents *HelpContents1;
    TAction *AboutAction;

    // Menu items
    TMainMenu *MainMenu1;
    TMenuItem *File1;
    TMenuItem *View1;
    TMenuItem *Help1;
    TMenuItem *Exit1;
    TMenuItem *Refresh1;
    TMenuItem *Contents1;
    TMenuItem *HelpN1;
    TMenuItem *About1;

    void __fastcall ListView1Compare(TObject *Sender, TListItem *Item1,
        TListItem *Item2, int Data, int &Compare);
    void __fastcall ListView1ColumnClick(TObject *Sender, TListColumn *Column);
    void __fastcall RefreshActionExecute(TObject *Sender);
    void __fastcall AboutActionExecute(TObject *Sender);

public:
    struct Item {
        CLSID clsid;
        UnicodeString name;

        std::vector<DMO_PARTIAL_MEDIATYPE> inputTypes;
        std::vector<DMO_PARTIAL_MEDIATYPE> outputTypes;
    };

    __fastcall TDMOListForm(TComponent *Owner);
    virtual __fastcall ~TDMOListForm();

    virtual void __fastcall AfterConstruction();

    void __fastcall Refresh();

    void add(const Item &data);

private:
    /**
     * <associates>Mediatypes.TMediaTypeDirectory</associates>
     */
    boost::scoped_ptr<TMediaTypeDirectory> MediaTypeDirectory;

    int sortColumnIndex;
    bool sortDescending;

    TThread *worker;

    /*
     * Updates the header of ListView1.
     */
    void __fastcall UpdateListView1Header();

    /*
     * Notifies that the refresh thread is terminated.
     */
    void __fastcall RefreshThreadTerminate(TObject *Sender);
};

class TRefreshThread : public TThread {
private:
    TDMOListForm *const ownerForm;

    TCriticalSection *SyncObj;
    std::vector<TDMOListForm::Item> QueuedItems;

public:
    __fastcall TRefreshThread(TDMOListForm *form, bool suspended);
    virtual __fastcall ~TRefreshThread();

protected:
    virtual void __fastcall Execute();

private:
    void __fastcall AddItems();
};

extern PACKAGE TDMOListForm *DMOListForm;

#endif
