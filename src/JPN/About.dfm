object AboutBox: TAboutBox
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = '%s '#12398#12496#12540#12472#12519#12531#24773#22577
  ClientHeight = 160
  ClientWidth = 300
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  DesignSize = (
    300
    160)
  PixelsPerInch = 96
  TextHeight = 15
  object Label1: TLabel
    Left = 48
    Top = 16
    Width = 244
    Height = 30
    Anchors = [akLeft, akTop, akRight]
    AutoSize = False
    Caption = '%s %s'#13#10'Copyright '#169' 2010-2013  Kaz Nishimura'
    WordWrap = True
  end
  object Label2: TLabel
    Left = 48
    Top = 60
    Width = 244
    Height = 45
    Anchors = [akLeft, akTop, akRight]
    AutoSize = False
    Caption = 
      'This program is free software: you can redistribute it and/or mo' +
      'dify it under the terms of the GNU General Public License.'
    WordWrap = True
  end
  object OKButton: TButton
    Left = 217
    Top = 127
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 0
  end
end
