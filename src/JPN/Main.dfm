object DMOListForm: TDMOListForm
  Left = 0
  Top = 0
  Caption = '%s'
  ClientHeight = 292
  ClientWidth = 426
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 15
  object ListView1: TListView
    Left = 0
    Top = 0
    Width = 426
    Height = 292
    Align = alClient
    Columns = <
      item
        Caption = #21517#21069
        Width = 100
      end
      item
        Caption = 'CLSID'
        Width = 100
      end
      item
        Caption = #20837#21147#12479#12452#12503
        Width = 100
      end
      item
        Caption = #20986#21147#12479#12452#12503
        Width = 100
      end>
    SortType = stData
    TabOrder = 0
    ViewStyle = vsReport
    OnColumnClick = ListView1ColumnClick
    OnCompare = ListView1Compare
  end
  object ActionList1: TActionList
    Left = 32
    Top = 32
    object FileExit1: TFileExit
      Category = 'File'
      Caption = #32066#20102'(&X)'
      Hint = 'Exit|Quits the application'
      ImageIndex = 43
    end
    object RefreshAction: TAction
      Category = 'View'
      Caption = '&Refresh'
      OnExecute = RefreshActionExecute
    end
    object HelpContents1: THelpContents
      Category = 'Help'
      Caption = '&Contents'
      ImageIndex = 40
    end
    object AboutAction: TAction
      Category = 'Help'
      Caption = #12496#12540#12472#12519#12531#24773#22577'(&A)'
      Hint = 'About|Describes the application'
      OnExecute = AboutActionExecute
    end
  end
  object MainMenu1: TMainMenu
    Left = 112
    Top = 32
    object File1: TMenuItem
      Caption = #12501#12449#12452#12523'(&F)'
      object Exit1: TMenuItem
        Action = FileExit1
      end
    end
    object View1: TMenuItem
      Caption = '&View'
      object Refresh1: TMenuItem
        Action = RefreshAction
      end
    end
    object Help1: TMenuItem
      Caption = #12504#12523#12503'(&H)'
      object Contents1: TMenuItem
        Action = HelpContents1
        Caption = '%s '#12398#12504#12523#12503'(&H)'
      end
      object HelpN1: TMenuItem
        Caption = '-'
      end
      object About1: TMenuItem
        Action = AboutAction
        Caption = '%s '#12398#12496#12540#12472#12519#12531#24773#22577'(&A)'
      end
    end
  end
end
