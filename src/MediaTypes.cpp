/*
 * MediaTypes - media type information (implementation)
 * Copyright (C) 2010-2013  Kaz Nishimura
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <vcl.h>
#pragma hdrstop

#include "MediaTypes.h"
#pragma package(smart_init)

#include "Strings.hpp"
#include <IOUtils.hpp>
#include <uuids.h>

UnicodeString __fastcall TMediaTypeDirectory::GetIniFileName() {
    return TPath::ChangeExtension(Application->ExeName, L"ini");
}

__fastcall TMediaTypeDirectory::TMediaTypeDirectory()
    : IniFile(new TIniFile(GetIniFileName())) {
}

TMediaType __fastcall TMediaTypeDirectory::Lookup(REFGUID Type, REFGUID Subtype)
{

    UnicodeString typeText = GUIDToString(Type);
    UnicodeString subtypeText = GUIDToString(Subtype);

    TMediaType mediaType;
    mediaType.Name = IniFile->ReadString(L"Subtypes", subtypeText, L"");
    if (mediaType.Name.IsEmpty()) {
        mediaType.Name = IniFile->ReadString(L"Types", typeText, L"");
        if (mediaType.Name.IsEmpty()) {
            if (Type == MEDIATYPE_Video) {
                mediaType.Name = L"Video";
            } else if (Type == MEDIATYPE_Audio) {
                mediaType.Name = L"Audio";
            }
        }
        mediaType.Name += L"/" + subtypeText;
    }
    return mediaType;
}
