/*
 * Package configuration for DMOList
 * Copyright (C) 2010-2014 Kaz Nishimura
 *
 * Copying and distribution of this file, with or without modification, are
 * permitted in any medium without royalty provided the copyright notice and
 * this notice are preserved.  This file is offered as-is, without any
 * warranty.
 */

#define PACKAGE_NAME "DMOList"
#define PACKAGE_VERSION "1.0.1"
#define PACKAGE_TAR_NAME "dmolist"
