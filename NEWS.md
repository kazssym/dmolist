# News and history of DMOList

## Version 1.0.1

Tag: release-1.0.1

Changes in this version are as follows:

* The projects have been upgraded for C++Builder XE4.
* A configuration for Win64 has been finally added.

## Version 1.0.0

Tag: release-1.0.0

This version is the first release of this program.
